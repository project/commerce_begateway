# Introduction

This module provides a Drupal Commerce payment method to embed the payment
services provided by payment service providers running on beGateway platform.

# Requirements

  * [Token](http://drupal.org/project/token)
  * Commerce Payment (from [Commerce](http://drupal.org/project/commerce) core)
  * Commerce Order (from [Commerce](http://drupal.org/project/commerce) core)
  * BeGateway API PHP Library (run `composer require begateway/begateway-api-php`)

# Installation

The module can be installed in several ways. It depends on what permissions you have got for your Drupal-site.

## Manually

1. [Download](https://www.drupal.org/project/ludwig) and install the `Ludwig` module by clicking the button `Install new module` link at the _Extend_ page.
2. Enable the `Ludwig` module
3. [Download](https://www.drupal.org/project/commerce_begateway) and install the `BeGateway Payment` module by clicking the button `Install new module` link at the _Extend_ page.
4. Visit _Reports -> Packages_ (admin/reports/packages) to download and install, if missing, the `begateway/begateway-api-php` library
5. Enable the `BeGateway Payment` module

## Composer

1. Run the command

```
composer require drupal/commerce_begateway
```

2. Enable the `BeGateway Payment` module

# Configuration

The module can be configured at the settings page _Commerce -> Configuration -> Payment_

# Test data

If you setup the module with default values, you can use the test data to make a test payment:

  * Shop ID ```361```
  * Secret key ```b8647b68898b084b836474ed8d61ffe117c9a01168d867f24953b776ddcb134d```
  * Checkout page domain ```checkout.begateway.com```

## Test card details

  * Card ```4200000000000000``` to get succesful payment
  * Card ```4005550000000019``` to get failed payment
  * Card name ```JOHN DOE```
  * Card expiry date ```01/30```
  * CVC ```123```

# Development

The module uses Docker to develop it. Execute commands to set up a developent environment:

  * setup a Drupal version in `docker-compose.yml`. `9` is by default
  * run `docker-compose up` to build and up Drupal
  * open `0.0.0.0` in your brower to install Drupal
  * visit the `Extend` menu to install Commerce
  * run `docker-compose exec drupal composer require begateway/begateway-api-php` to install the beGateway PHP library

# Описание

Данный модуль позволяет начать приём платежей в Drupal Commerce через провайдеров платежей, которые используют программное обеспечение beGateway.

# Требования

  * [Token](http://drupal.org/project/token)
  * Commerce Payment (из [Commerce](http://drupal.org/project/commerce) ядра)
  * Commerce Order (из [Commerce](http://drupal.org/project/commerce) ядра)
  * BeGateway API PHP библиотека (run `composer require begateway/begateway-api-php`)

# Установка

В зависимости от прав доступа к вашему Drupal-сайту, у вас есть несколько вариантов установки.

## Вручную

1. [Скачайте](https://www.drupal.org/project/ludwig) и установите модуль `Ludwig`, нажав кнопку `Установить новый модуль` в разделе _Расширения_
2. Включите модуль `Ludwig`
3. [Скачайте](https://www.drupal.org/project/commerce_begateway) и установите модуль `BeGateway Payment`, нажав кнопку `Установить новый модуль` в разделе _Расширения_
4. Перейдите в раздел _Отчёты -> Packages_ (admin/reports/packages), чтобы скачать и установить, если отсутствует, библиотеку `begateway/begateway-api-php`
5. Включите модуль `BeGateway Payment`

## Composer

1. Выполните на сервере команду

```
composer require drupal/commerce_begateway
```

2. Включите модуль `BeGateway Payment` в разделе _Расширения_

# Настройка

Модуль настраивается через настройки на странице _Торговля -> Конфигурация -> Оплата_

# Тестовые данные

Если вам еще не известны ваши настройки, то вы можете настроить модуль, используя демо-данные:

  * ID магазина ```361```
  * Секретный ключ магазина ```b8647b68898b084b836474ed8d61ffe117c9a01168d867f24953b776ddcb134d```
  * Домен страницы оплаты ```checkout.begateway.com```

### Тестовые карты

  * Карта ```4200000000000000``` для успешной оплаты
  * Карта ```4005550000000019``` для неуспешной оплаты
  * Имя на карте ```JOHN DOE```
  * Срок действия карты ```01/30```
  * CVC ```123```
