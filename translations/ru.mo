��          �      |      �     �     �       b        z     �     �  F   �  2   �     &     9     T  4   W     �     �     �     �     �  �   �     f     n  �  r          '  Z   >  �   �  1     5   L  C   �  {   �  j   B  /   �  ;   �     	  9    	     Z	     g	  (   �	     �	  ,   �	  �  �	     t     �                  	             
                                                                  Action Authorization Available tokens BeGateway service is not configured for use. Please contact an administrator to resolve this issue Enable ERIP Enable HALVA bankcard Enable bankcard Enter number of minutes within which a customer may complete a payment Enter payment page domain of your payment provider Enter your shop id Enter your shop secret key No Order # [commerce_order:order_number] at [site:name] Payment Payment description Payment page domain Shop Id Shop secrey key This description will be available in your payment provider interface. You can use token replacement patterns for adding order id, site name, etc Timeout Yes Project-Id-Version: 
PO-Revision-Date: 2022-04-02 16:04+0300
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: t
X-Poedit-SearchPath-0: .
 Тип операции Авторизация Доступные для использования токены-плэйсхолдеры Сервис оплаты BeGateway не настроен. Свяжитесь с администратором магазина Включить оплату через ЕРИП Включить оплату картой Халва Включить оплату банковскими картами Введите время в минутах, в течение которых заказ может быть оплачен Введите домен страницы оплаты вашего провайдера платежей Введите id вашего магазина Введите секретный ключ магазина Нет Заказ # [commerce_order:order_number] в [site:name] Платеж Описание платежа Домен страницы оплаты Id магазина Секретный ключ магазина Введите описание платежа, которое вы хотите видеть в личном кабинете вашего провайдера платежей у каждой операции. Вы можете использовать токены-плэйсхолдеры для передачи номера заказа, имени сайта и так далее Время на оплату Да 