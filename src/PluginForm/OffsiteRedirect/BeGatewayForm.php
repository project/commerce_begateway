<?php

namespace Drupal\commerce_begateway\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides the class for payment off-site form.
 *
 * Provide a buildConfigurationForm() method which calls buildRedirectForm()
 * with the right parameters.
 */
class BeGatewayForm extends BasePaymentOffsiteForm {

  const VERSION = '8.x-1.3';

  /**
   * Gateway plugin.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface
   */
  private $paymentGatewayPlugin;

  /**
   * Getting plugin's configuration.
   *
   * @param string $configuration
   *   Configuration name.
   *
   * @return mixed
   *   Configuration value.
   */
  private function getConfiguration($configuration) {
    return $this->paymentGatewayPlugin->getConfiguration()[$configuration];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $this->paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();

    $order        = $payment->getOrder();
    $total_price  = $order->getTotalPrice();
    $data         = array();

    \BeGateway\Settings::$shopId = $this->getConfiguration('shop_id');
    \BeGateway\Settings::$shopKey = $this->getConfiguration('shop_key');
    \BeGateway\Settings::$checkoutBase = 'https://' . $this->getConfiguration('checkout_domain');

    $transaction = new \BeGateway\GetPaymentToken;

    if ($this->getConfiguration('action') == 'authorization') {
      $transaction->setAuthorizationTransactionType();
    }

    $transaction->setTestMode($this->getConfiguration('mode') == 'test');

    $notification_url = $this->paymentGatewayPlugin->getNotifyUrl()->toString();
    $notification_url = str_replace('0.0.0.0', 'webhook.begateway.com:8443', $notification_url);

    $transaction->setNotificationUrl($notification_url);
    $transaction->setSuccessUrl($form['#return_url']);
    $transaction->setDeclineUrl($form['#return_url']);
    $transaction->setFailUrl($form['#return_url']);
    $transaction->setLanguage(\Drupal::languageManager()->getCurrentLanguage()->getId());
    $transaction->setTrackingId($payment->getOrderId());
    $transaction->money->setAmount($total_price ? $total_price->getNumber() : 0);
    $transaction->money->setCurrency($total_price->getCurrencyCode());
    $transaction->additional_data->setPlatformData('Drupal v.' . \Drupal::VERSION);
    $transaction->additional_data->setIntegrationData('BeGateway Commerce Module v.' . self::VERSION);

    $customer = $this->getBillingAddress($order);

    $transaction->customer->setEmail($customer['email']);
    $transaction->customer->setFirstName($customer['first_name']);
    $transaction->customer->setLastName($customer['last_name']);
    $transaction->customer->setCountry($customer['countryCode']);
    $transaction->customer->setAddress($customer['address1']);
    $transaction->customer->setCity($customer['city']);
    $transaction->customer->setZip($customer['postCode']);

    if ('yes' == $this->getConfiguration('enable_bankcard')) {
      $cc = new \BeGateway\PaymentMethod\CreditCard;
      $transaction->addPaymentMethod($cc);
    }

    if ('yes' == $this->getConfiguration('enable_halva')) {
      $halva = new \BeGateway\PaymentMethod\CreditCardHalva;
      $transaction->addPaymentMethod($halva);
    }

    if ('yes' == $this->getConfiguration('enable_erip')) {
      $erip = new \BeGateway\PaymentMethod\Erip(array(
        'order_id' => $order->id(),
        'account_number' => ltrim($order->id())
      ));
      $transaction->addPaymentMethod($erip);
    }

    $timeout = intval($this->getConfiguration('timeout'));

    if ($timeout > 0) {
      $offset = timezone_offset_get(timezone_open('UTC'), new \DateTime()); 
      $expired_at = date(\DateTime::ISO8601, ($timeout * 60 + time() + $offset));
      $transaction->setExpiryDate($expired_at);
    }
  
    $transaction->setDescription(
      \Drupal::token()->replace(
        $this->getConfiguration('description'), 
        ['commerce_order' => $this->getOrderNumber($order)]
      )
    );

    try {
      $response = $transaction->submit();
    } catch (Exception $e) { }

    if ($response->isSuccess()) {
      $redirect_url = $response->getRedirectUrlScriptName();
      $data['token'] = $response->getToken();
    } else {
      throw new PaymentGatewayException($response->getMessage());
    }

    return $this->buildRedirectForm($form, $form_state, $redirect_url, $data, 'post');
  }

  /**
   * Get the billing address for this order.
   *
   * @param Order $order
   *   The commerce order object.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *
   * @return array
   */
  private function getBillingAddress(OrderInterface $order) {
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $billingAddress = $order->getBillingProfile()->get('address')->first();
    $country_list  = \Drupal::service('address.country_repository')->getList();

    return [
      'email' => $order->get('mail')->first()->value,
      'first_name' => $billingAddress->getGivenName(),
      'last_name' => $billingAddress->getFamilyName(),
      'address1' => $billingAddress->getAddressLine1(),
      'address2' => $billingAddress->getAddressLine2(),
      'city' => $billingAddress->getLocality(),
      'postCode' => $billingAddress->getPostalCode(),
      'countryCode' => $billingAddress->getCountryCode(),
      'country' => $country_list[$billingAddress->getCountryCode()]
    ];
  }

  /**
   * Get the generated order number.
   *
   * Sets the value if not already set.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order with generated order number.
   */
  protected function getOrderNumber(OrderInterface $order) {
    if (!$order->getOrderNumber()) {
      $order_type_storage = \Drupal::entityTypeManager()->getStorage('commerce_order_type');

      /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
      $order_type = $order_type_storage->load($order->bundle());

      /** @var \Drupal\commerce_number_pattern\Entity\NumberPatternInterface $number_pattern */
      $number_pattern = $order_type->getNumberPattern();

      if ($number_pattern) {
        $order_number = $number_pattern->getPlugin()->generate($order);
      }
      else {
        $order_number = $order->id();
      }

      $order->setOrderNumber($order_number);
      $order->save();
    }

    return $order;
  }
}
