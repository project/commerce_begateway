<?php

namespace Drupal\commerce_begateway\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsNotificationsInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Serialization\Json;

/**
 * Provides the Off-site Redirect payment BeGateway gateway.
 *
 * @CommercePaymentGateway(
 *   id = "begateway_offsite_gateway",
 *   label = "BeGateway Payment Gateway",
 *   display_label = "BeGateway",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_begateway\PluginForm\OffsiteRedirect\BeGatewayForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 *   modes = {
 *     "test" = @Translation("Test"),
 *     "live" = @Translation("Live")
 *   }
 * )
 */
class BeGateway extends OffsitePaymentGatewayBase implements SupportsNotificationsInterface {

  use StringTranslationTrait;

  const STATUS_PAYMENT_AUTHORIZED = 'authorization';
  const STATUS_ORDER_COMPLETE = 'completed';
  const STATUS_PAYMENT_FAIL = 'authorization_voided';
  const STATUS_PAYMENT_REFUNDED = 'refunded';
  const STATUS_PAYMENT_PENDING = 'pending';
  const STATUS_PAYMENT_CANCELED = 'canceled';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config   = \Drupal::config('commerce_begateway.settings');
    $defaults = [];

    $defaults = $defaults + [
        'shop_id'  => $config->get('commerce_begateway.shop_id'),
        'shop_key' => $config->get('commerce_begateway.shop_key'),
        'checkout_domain' => $config->get('commerce_begateway.checkout_domain'),
        'timeout' => $config->get('commerce_begateway.timeout'),
        'action'      => $config->get('commerce_begateway.action'),
        'description' => $this->t('Order # [commerce_order:order_number] at [site:name]'),
        'enable_bankcard' => $config->get('commerce_begateway.enable_bankcard'),
        'enable_erip' => $config->get('commerce_begateway.enable_erip'),
        'enable_halva' => $config->get('commerce_begateway.enable_halva')

      ];
    return $defaults + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['shop_id'] = [
      '#title'         => $this->t('Shop Id'),
      '#description'   => $this->t('Enter your shop id'),
      '#default_value' => $this->configuration['shop_id'],
      '#type'          => 'textfield',
      '#required'      => TRUE
    ];

    $form['shop_key'] = [
      '#title'         => $this->t('Shop secrey key'),
      '#description'   => $this->t('Enter your shop secret key'),
      '#default_value' => $this->configuration['shop_key'],
      '#type'          => 'textfield',
      '#required'      => TRUE
    ];

    $form['action'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Action'),
      '#default_value' => $this->configuration['action'],
      '#options'       => array(
        'payment'       => $this->t('Payment'),
        'authorization' => $this->t('Authorization')
      ),
      '#required'      => TRUE
    ];

    $form['checkout_domain'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Payment page domain'),
      '#description'   => $this->t('Enter payment page domain of your payment provider'),
      '#default_value' => $this->configuration['checkout_domain'],
      '#required'      => TRUE
    ];

    $form['description'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Payment description'),
      '#description'   => $this->t('This description will be available in your payment provider interface. You can use token replacement patterns for adding order id, site name, etc'),
      '#default_value' => $this->configuration['description'],
      '#required'      => TRUE
    ];

    $form['timeout'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Timeout'),
      '#description'   => $this->t('Enter number of minutes within which a customer may complete a payment'),
      '#default_value' => $this->configuration['timeout'],
      '#required'      => TRUE
    ];

    $form['enable_bankcard'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Enable bankcard'),
      '#default_value' => $this->configuration['enable_bankcard'],
      '#options'       => array(
        'yes'          => $this->t('Yes'),
        'no'           => $this->t('No')
      ),
      '#required'      => FALSE
    ];

    $form['enable_erip'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Enable ERIP'),
      '#default_value' => $this->configuration['enable_erip'],
      '#options'       => array(
        'no'           => $this->t('No'),
        'yes'          => $this->t('Yes')
      ),
      '#required'      => FALSE
    ];

    $form['enable_halva'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Enable HALVA bankcard'),
      '#default_value' => $this->configuration['enable_halva'],
      '#options'       => array(
        'no'           => $this->t('No'),
        'yes'          => $this->t('Yes')
      ),
      '#required'      => FALSE
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $form['token_help'] = [
        '#type'  => 'details',
        '#title' => $this->t('Available tokens'),
      ];

      $form['token_help']['token_tree_link'] = [
        '#theme'       => 'token_tree_link',
        '#token_types' => ['commerce_order'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $required_fields = [
      'shop_id',
      'shop_key',
      'action',
      'description',
      'checkout_domain'
    ];
    foreach ($required_fields as $key) {
      if (empty($values[$key])) {
        $this->messenger->addError($this->t('BeGateway service is not configured for use. Please contact an administrator to resolve this issue'));
        return FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values                                  = $form_state->getValue($form['#parents']);
      $this->configuration['shop_id']          = $values['shop_id'];
      $this->configuration['shop_key']         = $values['shop_key'];
      $this->configuration['action']           = $values['action'];
      $this->configuration['checkout_domain']  = $values['checkout_domain'];
      $this->configuration['description']      = $values['description'];
      $this->configuration['timeout']          = $values['timeout'];
      $this->configuration['enable_bankcard']  = $values['enable_bankcard'];
      $this->configuration['enable_erip']      = $values['enable_erip'];
      $this->configuration['enable_halva']     = $values['enable_halva'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    \BeGateway\Settings::$shopId  = $this->configuration['shop_id'];
    \BeGateway\Settings::$shopKey = $this->configuration['shop_key'];
    \BeGateway\Settings::$checkoutBase = 'https://' . $this->configuration['checkout_domain'];

    $uid = $request->query->get('uid');

    if (is_null($uid)) {
      throw new PaymentGatewayException('Payment failed');
    }

    $query = new \BeGateway\QueryByUid;
    $query->setUid($request->query->get('uid'));

    $query_response = $query->submit();
    \Drupal::logger('commerce_payment')->notice('BeGateway Query response for order id: @order. Response: @response',
      array(
        '@order' => $query_response->getTrackingId(),
        '@response' => print_r($query_response, true)
      )
    );

    $status = $query_response->isSuccess() || $query_response->isPending();

    if ($this->isPaymentValid($order, $query_response) == false || $status != true) {
      \Drupal::logger('commerce_payment')->notice('BeGateway response onReturn for order id: @order. Mode: @mode. Status: @status. Message: @message',
        array(
          '@mode' => $query_response->isTest() ? 'test' : 'live',
          '@order' => $query_response->getTrackingId(),
          '@status' => $query_response->getStatus(),
          '@message' => $query_response->getMessage()
        )
      );
      #return $this->onCancel($order, $request);
      throw new PaymentGatewayException('Payment failed');
    }
  }

  /**
   * Getting configuration.
   *
   * @return array
   *   Configuration array.
   */
  public function getStoreDataConfiguration() {
    $config = \Drupal::config('commerce_begateway.settings');

    return $store_data = [
      'shop_id'         => isset($this->configuration['shop_id']) ? $this->configuration['shop_id'] : $config->get('commerce_begateway.shop_id'),
      'shop_key'        => isset($this->configuration['shop_key']) ? $this->configuration['shop_key'] : $config->get('commerce_begateway.shop_key'),
      'action'          => isset($this->configuration['action']) ? $this->configuration['action'] : $config->get('commerce_begateway.action'),
      'checkout_domain' => isset($this->configuration['checkout_domain']) ? $this->configuration['checkout_domain'] : $config->get('commerce_begateway.checkout_domain'),
      'timeout'         => isset($this->configuration['timeout']) ? $this->configuration['timeout'] : $config->get('commerce_begateway.timeout'),
      'enable_bankcard' => isset($this->configuration['enable_bankcard']) ? $this->configuration['enable_bankcard'] : $config->get('commerce_begateway.enable_bankcard'),
      'enable_erip'     => isset($this->configuration['enable_erip']) ? $this->configuration['enable_erip'] : $config->get('commerce_begateway.enable_erip'),
      'enable_halva'    => isset($this->configuration['enable_halva']) ? $this->configuration['enable_halva'] : $config->get('commerce_begateway.enable_halva')
    ];
  }

  /** Validate order data with BeGateway transaction data
   * @params $order
   * @params $transaction
   *
   * @return bool
  */
  public function isPaymentValid($order, $transaction) {

    if (is_null($order) || is_null($transaction)) {
      return false;
    }

    $uid = $transaction->getUid();
    if (!isset($uid)) {
      return false;
    }

    $money = new \BeGateway\Money;
    $money->setCurrency($order->getTotalPrice()->getCurrencyCode());
    $money->setAmount(number_format($order->getTotalPrice()->getNumber(), 2, '.', ''));
    if (!$transaction->hasTransactionSection()) {
      return false;
    }

    if ($transaction->getResponse()->transaction->amount != $money->getCents()) {
      return false;
    }

    if ($transaction->getResponse()->transaction->currency != $money->getCurrency()){
      return false;
    }

    if ($transaction->getTrackingId() != $order->id()) {
      return false;
    }
    return true;
  }

  /**
   * IPN callback.
   *
   * IPN will be called after a succesful payment or changed state
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   The response, or NULL to return an empty HTTP 200 response
   */
  public function onNotify(Request $request) {
    \BeGateway\Settings::$shopId = $this->configuration['shop_id'];
    \BeGateway\Settings::$shopKey = $this->configuration['shop_key'];
    $webhook = new \BeGateway\Webhook();

    if (!$webhook->isAuthorized()) {
      return new Response('Not authorized', 200);
    }

    $order_id = $webhook->getResponse()->transaction->tracking_id;
    $order = Order::load($order_id);

    if ($this->isPaymentValid($order, $webhook) != true) {
      return new Response('Invalid transaction', 200);
    }

    $payment_storage = \Drupal::service('entity_type.manager')
      ->getStorage('commerce_payment');

    $state = null;

    if ($webhook->isSuccess()) {
      $state = self::STATUS_ORDER_COMPLETE;
    }

    if ($webhook->isFailed()) {
      $state = self::STATUS_PAYMENT_FAIL;
    }

    if ($webhook->isPending()) {
      $state = self::STATUS_PAYMENT_PENDING;
    }

    if (is_null($state)) {
      return new Response('No status to update', 200);
    }

    if ($webhook->getStatus() == 'expired' || $webhook->isFailed()) {
      $order->set('state', self::STATUS_PAYMENT_CANCELED);
      $order->save();
    }

    $last = $payment_storage->loadByProperties([
      'payment_gateway' => $this->entityId,
      'order_id' => $order_id,
      'remote_id' => $webhook->getUid()
    ]);
    if (!empty($last)) {
      $payment_storage->delete($last);
    }

    $payment         = $payment_storage->create([
      'state'           => $state,
      'amount'          => $order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id'        => $order_id,
      'test'            => $webhook->isTest(),
      'remote_id'       => $webhook->getUid(),
      'remote_state'    => $webhook->getStatus(),
    ]);
    $payment->save();

    if ($order->get('state') != self::STATUS_ORDER_COMPLETE && $webhook->isSuccess()) {
      $order->set('state', self::STATUS_ORDER_COMPLETE);
      $order->save();
    }

    \Drupal::logger('commerce_payment')->notice('BeGateway response for order id: @order. Mode: @mode. Status: @status. Message: @message',
      array(
        '@mode' => $webhook->isTest() ? 'test' : 'live',
        '@order' => $order_id,
        '@status' => $webhook->getStatus(),
        '@message' => $webhook->getMessage()
      ));

    return new Response('Received new payment status', 200);
  }
}
